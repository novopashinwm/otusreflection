using System;
using System.Diagnostics;

namespace OtusReflection
{
    public static class MySerialization
    {
        private static TestData _test;
        public static void Deserialize(string json, int times)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < times; i++)
                json.FromJson<TestData>();
            
            watch.Stop();
            Console.WriteLine($"Время на десериализацию {watch.ElapsedMilliseconds}");
        }

        public static string Serialize(int times)
        {
            _test = TestData.Get();
            string json = String.Empty;
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < times; i++)
                json = _test.ToJson();
            
            watch.Stop();
            Console.WriteLine(json);
            Console.WriteLine($"Время на сериализацию {watch.ElapsedMilliseconds}");
            return json;
        }
    }
}