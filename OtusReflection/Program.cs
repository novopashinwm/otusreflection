﻿using System;

namespace OtusReflection
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string json = string.Empty;
            int times = 100000;

            Console.WriteLine("Cериализуемый класс: class Data { int i1, i2, i3, i4, i5;}");
            Console.WriteLine("Код сериализации-десериализации:");
            Console.WriteLine($"Количество замеров: {times} итераций");
            Console.WriteLine();
            Console.WriteLine("Мой рефлекш:");
            json = MySerialization.Serialize(times);
            MySerialization.Deserialize(json, times);
            Console.WriteLine();
            Console.WriteLine("Стандартный механизм (NewtonsoftJson):");
            json = StandardSerialization.Serialize(times);
            StandardSerialization.Deserialization(json,times);
            Console.ReadKey();

        }
    }
}