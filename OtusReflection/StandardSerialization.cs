using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace OtusReflection
{
    public static class StandardSerialization
    {
        private static TestData _test;
        public static void Deserialization(string json, int times)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < times; i++)
               JsonConvert.DeserializeObject<TestData>(json);
            watch.Stop();
            Console.WriteLine($"Время на десериализацию {watch.ElapsedMilliseconds}");
        }

        public static string Serialize(int times)
        {
            _test = TestData.Get();
            string json = String.Empty;
            Stopwatch watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < times; i++)
                json = JsonConvert.SerializeObject(_test);
            Console.WriteLine(json);
            Console.WriteLine($"Время на сериализацию {watch.ElapsedMilliseconds}");
            return json;
        }
    }
}